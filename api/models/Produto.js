const mongoose = require('mongoose')

const ProdutoSchema = new mongoose.Schema({
    nome: {type: String},
    fabricante: {type: String},
    valor: {type: Number},
})

mongoose.model("Produto", ProdutoSchema)