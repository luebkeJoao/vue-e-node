const http = axios.create({
    headers: {
        Authorization: {
            toString () {
                return `Bearer ${localStorage.getItem('token')}`
            }
        },
    }
});
export default http;